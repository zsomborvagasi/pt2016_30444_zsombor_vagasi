package PT2016.First.First;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.FlowLayout;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import java.awt.GridLayout;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;


//this class handles the coefficient setting for polinomials
public class SetCoeff extends JFrame {

	private JPanel contentPane;
	
	public int degreeFirst;
	public int degreeSecond;
	JTextField[] coeffFirstField;
	JTextField[] coeffSecondField;
	JLabel[] coeffFirstLabel;
	JLabel[] coeffSecondLabel;
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SetCoeff frame = new SetCoeff(0,0);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public SetCoeff(int first,int second) {
		super("Enter Coefficients");
		setVisible(true);
		setSize(600,300);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(true);
		setLocationRelativeTo(null);
		// grid layout for 2 panels and a button, each panel will contain one polinomial
		getContentPane().setLayout(new GridLayout(3,10));
		
		JPanel panel = new JPanel();
		getContentPane().add(panel);
		
		panel.setLayout(new FlowLayout());
		
		getContentPane().add(panel);
		
		JPanel panel_1 = new JPanel();
		getContentPane().add(panel_1);
		
		panel_1.setLayout(new FlowLayout());
		getContentPane().add(panel_1);
		
		setFirstDegree(first);
		setSecondDegree(second);
		
		// we have to create the labels and the text fields to represent the polinomial
		// there is always one more label than the degree number, we have polinomial with 0 degree remember
		coeffFirstLabel = new JLabel[degreeFirst + 1];
		coeffFirstField = new JTextField[degreeFirst + 1];
		JLabel firstHeader = new JLabel("P(x) = ");
		panel.add(firstHeader);
		
		// after adding the header, we create as many text fields and labels as the degree of the ppolinomial, plus one
		// text field is set to be able to be able to display two digit numbers
		for (int i = degreeFirst; i>=0; i--)
		{
			coeffFirstField[i] = new JTextField("0",2);
			panel.add(coeffFirstField[i]);
			coeffFirstLabel[i] = new JLabel("x^" + Integer.toString(i));
			panel.add(coeffFirstLabel[i]);
		}
		
		// same procedure as with the first polinomial
		coeffSecondLabel = new JLabel[degreeSecond + 1];
		coeffSecondField = new JTextField[degreeSecond + 1];
		JLabel secondHeader = new JLabel("Q(x) = ");
		panel_1.add(secondHeader);
		
		for (int i = degreeSecond; i>=0; i--)
		{
			coeffSecondField[i] = new JTextField("0",2);
			panel_1.add(coeffSecondField[i]);
			coeffSecondLabel[i] = new JLabel("x^" + Integer.toString(i));
			panel_1.add(coeffSecondLabel[i]);
		}
		
		
		//application generated event handler for mouse click on Submit Coeff button
		// the method will parse each coeff for the respective polinomial, turn the coeff into a double vector object
		// and pass the degree and coeffvar of the polinomials as Calculon objects to the Calculator class
		// which takes as parameter two calculon objects
		JButton btnNewButton = new JButton("Submit Coeff");
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) 
			{
				double[] coeffVarFirst = new double[degreeFirst + 1];
				for (int i = degreeFirst; i>= 0; i--)
				{
					coeffVarFirst[i] = Integer.parseInt(coeffFirstField[i].getText());
				}
				Calculon calcFirst = new Calculon(degreeFirst,coeffVarFirst);
				
				double[] coeffVarSecond = new double[degreeSecond + 1];
				for (int i = degreeSecond; i>= 0; i--)
				{
					coeffVarSecond[i] = Integer.parseInt(coeffSecondField[i].getText());
				}
				Calculon calcSecond = new Calculon(degreeSecond,coeffVarSecond);
				
				Calculator calc = new Calculator(calcFirst,calcSecond);
				setVisible(false);
			}
		});
		getContentPane().add(btnNewButton);
	}
	
	public void setFirstDegree(int degree)
	{
		degreeFirst = degree;
	}
	
	public void setSecondDegree(int degree)
	{
		degreeSecond = degree;
	}

}
