package PT2016.First.First;

import org.junit.*;
import static org.junit.Assert.assertEquals;
import org.junit.runner.*;

public class JUnitTester {

	//unit test for addition
	//first poli : 1*X^1 + 1*X^0
	//second poli : 1*X^1 + 1*X^0
	//result poli : 2*X^1 + 2*X^0
	@Test 
	public void testForAdd()
	{
	    Calculon calc = new Calculon();
	    
	    int poli1Degree = 1;
	    double[] poli1CoeffVar = {1.0,1.0};
		Calculon poli1 = new Calculon(poli1Degree,poli1CoeffVar);
		
		int poli2Degree = 1;
	    double[] poli2CoeffVar = {1.0,1.0};
		Calculon poli2 = new Calculon(poli2Degree,poli2CoeffVar);
		
		int poliResultDegree = 1;
	    double[] poliResultCoeffVar = {2.0,2.0};
		Calculon poliResult = new Calculon(poliResultDegree,poliResultCoeffVar);		
		
		Calculon resultContainer = new Calculon (0,null);
		resultContainer.classCoeffVar = calc.addition(poli1, poli2).classCoeffVar;
		resultContainer.classDegree = calc.addition(poli1, poli2).classDegree;
		
		if (poliResult.classCoeffVar.equals(resultContainer.classCoeffVar))
		{
			System.out.println("it should be equal");
		}
		
		assertEquals(poliResult.classDegree, resultContainer.classDegree);
		assertEquals(poliResult.classCoeffVar, resultContainer.classCoeffVar);
	}
	
	
	//unit test for substraction
		//first poli : 1*X^1 + 1*X^0
		//second poli : 1*X^1 + 1*X^0
		//result poli : 0*X^1 + 0*X^0
	@Test 
	public void testForSubstract()
	{
	    Calculon calc = new Calculon();
	    
	    int poli1Degree = 1;
	    double[] poli1CoeffVar = {1.0,1.0};
		Calculon poli1 = new Calculon(poli1Degree,poli1CoeffVar);
		
		int poli2Degree = 1;
	    double[] poli2CoeffVar = {1.0,1.0};
		Calculon poli2 = new Calculon(poli2Degree,poli2CoeffVar);
		
		int poliResultDegree = 1;
	    double[] poliResultCoeffVar = {0.0,0.0};
		Calculon poliResult = new Calculon(poliResultDegree,poliResultCoeffVar);
		
		assertEquals(poliResult.classCoeffVar, calc.substract(poli1, poli2).classCoeffVar);
		assertEquals(poliResult.classDegree, calc.substract(poli1, poli2).classDegree);
	}
	
	//unit test for derivate
		//first poli : 3*X^1 + 2*X^0
		//result poli : 3*X^0
	@Test 
	public void testForDerivate()
	{
	    Calculon calc = new Calculon();
	    
	    int poli1Degree = 1;
	    double[] poli1CoeffVar = {2.0,3.0};
		Calculon poli1 = new Calculon(poli1Degree,poli1CoeffVar);
		
		int poliResultDegree = 0;
	    double[] poliResultCoeffVar = {3.0};
		Calculon poliResult = new Calculon(poliResultDegree,poliResultCoeffVar);
		
		Calculon resultContainer = new Calculon (0,null);
		resultContainer.classCoeffVar = calc.derivate(poli1).classCoeffVar;
		resultContainer.classDegree = calc.derivate(poli1).classDegree;
		
		assertEquals(poliResult.classCoeffVar, resultContainer.classCoeffVar);
		assertEquals(poliResult.classDegree, resultContainer.classDegree);
		
	}
	
	//unit test for derivate
			//first poli : 3*X^1 + 2*X^0
			//result poli : 1,5*X^2 + 2*x^1 
		@Test 
		public void testForIntegrate()
		{
		    Calculon calc = new Calculon();
		    
		    int poli1Degree = 1;
		    double[] poli1CoeffVar = {2.0,3.0};
			Calculon poli1 = new Calculon(poli1Degree,poli1CoeffVar);
			
			int poliResultDegree = 2;
		    double[] poliResultCoeffVar = {0,2.0,1.5};
			Calculon poliResult = new Calculon(poliResultDegree,poliResultCoeffVar);
			
			Calculon resultContainer = new Calculon (0,null);
			resultContainer.classCoeffVar = calc.integrate(poli1).classCoeffVar;
			resultContainer.classDegree = calc.integrate(poli1).classDegree;
			
			assertEquals(poliResult.classCoeffVar, resultContainer.classCoeffVar);
			assertEquals(poliResult.classDegree, resultContainer.classDegree);
			
		}
		
		
		//unit test for multiplication
		//first poli : 2*X^1 + 1*X^0
		//second poli : 5*x^2 + 4*X^1 + 3*X^0
		//result poli : 10*x^3 + 13*x^2 + 10*X^1 + 3*X^0
	@Test 
	public void testForMultiply()
	{
	    Calculon calc = new Calculon();
	    
	    int poli1Degree = 1;
	    double[] poli1CoeffVar = {1.0,2.0};
		Calculon poli1 = new Calculon(poli1Degree,poli1CoeffVar);
		
		int poli2Degree = 2;
	    double[] poli2CoeffVar = {3.0,4.0,5.0};
		Calculon poli2 = new Calculon(poli2Degree,poli2CoeffVar);
		
		int poliResultDegree = 3;
	    double[] poliResultCoeffVar = {3,10,13,10};
		Calculon poliResult = new Calculon(poliResultDegree,poliResultCoeffVar);
		
		assertEquals(poliResult.classCoeffVar, calc.multiply(poli1, poli2).classCoeffVar);
		assertEquals(poliResult.classDegree, calc.multiply(poli1, poli2).classDegree);
	}
}
