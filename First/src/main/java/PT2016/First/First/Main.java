package PT2016.First.First;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.AbstractAction;
import java.awt.event.ActionEvent;
import javax.swing.Action;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.GridLayout;


//this class starts the application and handles degree setting for polinomials
public class Main extends JFrame {

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Main frame = new Main();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Main() {
		super("Select Degree");
		setSize(600,300);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(true);
		setLocationRelativeTo(null);
		
		// Five rows to accomodate the two labels, the two combo boxes and the button
		GridLayout grid = new GridLayout(5,8);
		getContentPane().setLayout(grid);
		
		JLabel lblNewLabel = new JLabel("Select Degree of First Polynomial");
		getContentPane().add(lblNewLabel);
		
		final JComboBox comboBox = new JComboBox();
		comboBox.setEditable(true);
		getContentPane().add(comboBox);
		
		JLabel lblNewLabel_1 = new JLabel("Select Degree of Second Polynomial");
		getContentPane().add(lblNewLabel_1);
		
		final JComboBox comboBox_1 = new JComboBox();
		getContentPane().add(comboBox_1);
		
		//application generated mouse click event for the Submit button
		//code inside acquires degree of polinomials and calls SetCoeff class passing these degrees as parameters
		JButton btnNewButton = new JButton("Submit");
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				String firstDegree = comboBox.getSelectedItem().toString();
				String secondDegree = comboBox_1.getSelectedItem().toString();
				SetCoeff setCoeff = new SetCoeff(Integer.parseInt(firstDegree),Integer.parseInt(secondDegree));
				setVisible(false);
			}
		});
		getContentPane().add(btnNewButton);
		
		// combo boxes filled with default values for degrees
		comboBox.addItem("0");
		comboBox.addItem("1");
		comboBox.addItem("2");
		comboBox.addItem("3");
		comboBox.addItem("4");
		comboBox.addItem("5");
		comboBox.addItem("6");
		comboBox.addItem("7");
		comboBox.addItem("8");
		comboBox.addItem("9");
		
		comboBox_1.addItem("0");
		comboBox_1.addItem("1");
		comboBox_1.addItem("2");
		comboBox_1.addItem("3");
		comboBox_1.addItem("4");
		comboBox_1.addItem("5");
		comboBox_1.addItem("6");
		comboBox_1.addItem("7");
		comboBox_1.addItem("8");
		comboBox_1.addItem("9");
	}

}
