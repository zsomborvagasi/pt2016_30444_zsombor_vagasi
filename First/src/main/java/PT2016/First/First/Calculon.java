package PT2016.First.First;

//This class handles all the features related to polinomial operations
public class Calculon {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	
	public int classDegree;
	public double[] classCoeffVar;
	
	//overloaded method taking degree and coeffvar parameters to represent a polinomial
	public Calculon(int degree, double[] coeffVar)
	{
		classDegree = degree;
		classCoeffVar = coeffVar;
	}
	
	//constructor
	public Calculon()
	{
		
	}

	//method to add two polinomials
	public Calculon addition(Calculon first, Calculon second)
	{
		int resultDegree = 0;
		double[] resultCoeffVar = null;
		//decide which polinomial has a higher degree, continue accordingly, because we want to avoid out of array exception
		if (first.classDegree >= second.classDegree)
		{
			resultDegree = first.classDegree;
			resultCoeffVar = new double[resultDegree + 1];
			// I like the idea to go backwards with a for, first taking care of the item with the highest degree
			for (int i = first.classDegree;i>=0; i--)
			{
				if (i <= second.classDegree)
				{
					resultCoeffVar[i] = first.classCoeffVar[i] + second.classCoeffVar[i];
				}
				else
				{
					resultCoeffVar[i] = first.classCoeffVar[i];
				}
			}
		}
		else
		{
			resultDegree = second.classDegree;
			resultCoeffVar = new double[resultDegree + 1];
			for (int i = second.classDegree;i>=0; i--)
			{
				if (i <= first.classDegree)
				{
					resultCoeffVar[i] = first.classCoeffVar[i] + second.classCoeffVar[i];
				}
				else
				{
					resultCoeffVar[i] = second.classCoeffVar[i];
				}
			}
		}
		Calculon result = new Calculon(resultDegree,resultCoeffVar);
		return result;
	}
	
	// same method as in the Addition class
	public Calculon substract(Calculon first, Calculon second)
	{
		int resultDegree = 0;
		double[] resultCoeffVar = null;
		if (first.classDegree >= second.classDegree)
		{
			resultDegree = first.classDegree;
			resultCoeffVar = new double[resultDegree + 1];
			for (int i = first.classDegree;i>=0; i--)
			{
				if (i <= second.classDegree)
				{
					resultCoeffVar[i] = first.classCoeffVar[i] - second.classCoeffVar[i];
				}
				else
				{
					resultCoeffVar[i] = 0 - first.classCoeffVar[i];
				}
			}
		}
		else
		{
			resultDegree = second.classDegree;
			resultCoeffVar = new double[resultDegree + 1];
			for (int i = second.classDegree;i>=0; i--)
			{
				if (i <= first.classDegree)
				{
					resultCoeffVar[i] = first.classCoeffVar[i] - second.classCoeffVar[i];
				}
				else
				{
					resultCoeffVar[i] = 0 - second.classCoeffVar[i];
				}
			}
		}
		Calculon result = new Calculon(resultDegree,resultCoeffVar);
		return result;
	}
	
	// method to handle polinomial multiplication
	public Calculon multiply(Calculon first, Calculon second)
	{
		int multiplyDegree = first.classDegree + second.classDegree;
		int resultDegree = multiplyDegree;
		
		//initialization of result polinomial is necessary because of calculation of coeffVars,
		// cannot add to a null item inside for-for
		double[] resultCoeffVar = new double[multiplyDegree + 1];
		for (int i = 0; i<= multiplyDegree; i++)
		{
			resultCoeffVar[i] = 0;
		}
		
		// for inside for to iterate through the two polinomials, 
		// if there will be an element with the same degree as before, it simply adds the coeffvar to the
		// previous value and goes further
		for (int i = first.classDegree;i>= 0; i--)
		{
			for (int in = second.classDegree; in>=0;in --)
			{
				resultCoeffVar[i+in] = resultCoeffVar[i+in] + (first.classCoeffVar[i] * second.classCoeffVar[in]);
			}
		}
		
		Calculon result = new Calculon (resultDegree, resultCoeffVar);
		return result;
	}
	
	//Couldnt find an algorithm for this, skeleton method kept for future development if necessary
	public Calculon divide(Calculon first, Calculon second)
	{
		if ((second.classDegree == 0) && (second.classCoeffVar[0] == 0) )
		{
			throw new RuntimeException ("Divison by zero polinomial");
		}
		int resultDegree = 0;
		double[] resultCoeffVar = null;

		
		if (first.classDegree >= second.classDegree)
		{
			// TODO: divide two polinoms
		}
		else
		{
			resultCoeffVar = new double[0];
			resultCoeffVar[0] = 0;
		}
	
		
		Calculon result = new Calculon (resultDegree, resultCoeffVar);
		return result;
	}
	
	//method to handle derivate functionality
	public Calculon derivate(Calculon poli)
	{
		
		int resultDegree = 0;
		double[] resultCoeffVar = null;
		//if poli is zero degree, return zero poli
		if (poli.classDegree == 0)
		{
			resultDegree = 0;
			resultCoeffVar = new double[1];
			resultCoeffVar[0] = 0;
		}
		else
			//algorithm to calculate the new degree of the derivate polinomial
		{
			resultCoeffVar = new double[poli.classDegree];
			resultDegree = poli.classDegree - 1;
			for (int i = poli.classDegree; i>= 0; i--)
			{
				if ((i-1) >= 0)
				{
					resultCoeffVar[i - 1] = poli.classCoeffVar[i] * i;
				}
			}
		}
		
		Calculon result = new Calculon (resultDegree, resultCoeffVar);
		return result;
	}
	
	//method to handle integrate operation for polinomial
	public Calculon integrate (Calculon poli)
	{
		int resultDegree = 0;
		double[] resultCoeffVar = null;
		
		// if zero degree poli then return zero degree
		if (poli.classDegree == 0)
		{
			resultCoeffVar = new double[1];
			resultCoeffVar[0] = 0;
		}
		else
			//algorithm to calculate integrate of poli
		{
			resultDegree = poli.classDegree + 1;
			resultCoeffVar = new double[resultDegree + 1];
			for (int i = resultDegree;i>0;i--)
			{
				resultCoeffVar[i] = poli.classCoeffVar[i - 1] / i;
			}
		}
		Calculon result = new Calculon (resultDegree, resultCoeffVar);
		return result;
	}
}
