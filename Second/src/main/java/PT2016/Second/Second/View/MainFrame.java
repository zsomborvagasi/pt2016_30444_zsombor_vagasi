package PT2016.Second.Second.View;

import PT2016.Second.Second.Engine.*;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JTextField;

public class MainFrame extends JFrame {

	JPanel panel;
	private JTextField textField_0;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;
	private JTextField textField_6;
	private JTextField textField_7;
	ArrayList<JTextField> queue1;
	ArrayList<JTextField> queue2;
	ArrayList<JTextField> queue3;
	ArrayList<JTextField> queue4;
	ArrayList<JTextField> queue5;
	ArrayList<ArrayList<JTextField>> queue;
	private JTextField textField_00;
	private JTextField textField_01;
	private JTextField textField_02;
	private JTextField textField_03;
	private JTextField textField_04;
	private JTextField textField_05;
	private JTextField textField_06;
	private JTextField textField_07;
	private JTextField textField_10;
	private JTextField textField_11;
	private JTextField textField_12;
	private JTextField textField_13;
	private JTextField textField_14;
	private JTextField textField_15;
	private JTextField textField_16;
	private JTextField textField_17;
	private JTextField textField_20;
	private JTextField textField_21;
	private JTextField textField_22;
	private JTextField textField_23;
	private JTextField textField_24;
	private JTextField textField_25;
	private JTextField textField_26;
	private JTextField textField_27;
	private JTextField textField_30;
	private JTextField textField_31;
	private JTextField textField_32;
	private JTextField textField_33;
	private JTextField textField_34;
	private JTextField textField_35;
	private JTextField textField_36;
	private JTextField textField_37;

	/**
	 * Create the panel.
	 */
	public MainFrame() {
		super("Server stuff");
		setSize(1000, 800);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(true);
		setLocationRelativeTo(null);
		setVisible(true);
		panel = new JPanel();
		getContentPane().add(panel);
		panel.setLayout(null);

		
		textField_0 = new JTextField();
		textField_0.setBounds(41, 58, 86, 20);
		panel.add(textField_0);
		textField_0.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setBounds(152, 58, 86, 20);
		panel.add(textField_1);
		textField_1.setColumns(10);
		
		textField_2 = new JTextField();
		textField_2.setBounds(259, 58, 86, 20);
		panel.add(textField_2);
		textField_2.setColumns(10);
		
		textField_3 = new JTextField();
		textField_3.setBounds(368, 58, 86, 20);
		panel.add(textField_3);
		textField_3.setColumns(10);
		
		textField_4 = new JTextField();
		textField_4.setBounds(476, 58, 86, 20);
		panel.add(textField_4);
		textField_4.setColumns(10);
		
		textField_5 = new JTextField();
		textField_5.setBounds(582, 58, 86, 20);
		panel.add(textField_5);
		textField_5.setColumns(10);
		
		textField_6 = new JTextField();
		textField_6.setBounds(700, 58, 86, 20);
		panel.add(textField_6);
		textField_6.setColumns(10);
		
		textField_7 = new JTextField();
		textField_7.setBounds(812, 58, 86, 20);
		panel.add(textField_7);
		textField_7.setColumns(10);
		
		queue1 = new ArrayList<JTextField>();
		queue = new ArrayList<ArrayList<JTextField>>();
		queue1.add(0,textField_0);
		queue1.add(1,textField_1);
		queue1.add(2,textField_2);
		queue1.add(3,textField_3);
		queue1.add(4,textField_4);
		queue1.add(5,textField_5);
		queue1.add(6,textField_6);
		queue1.add(7,textField_7);
		
		textField_00 = new JTextField();
		textField_00.setBounds(41, 115, 86, 20);
		panel.add(textField_00);
		textField_00.setColumns(10);
		
		textField_01 = new JTextField();
		textField_01.setBounds(152, 115, 86, 20);
		panel.add(textField_01);
		textField_01.setColumns(10);
		
		textField_02 = new JTextField();
		textField_02.setBounds(259, 115, 86, 20);
		panel.add(textField_02);
		textField_02.setColumns(10);
		
		textField_03 = new JTextField();
		textField_03.setBounds(368, 115, 86, 20);
		panel.add(textField_03);
		textField_03.setColumns(10);
		
		textField_04 = new JTextField();
		textField_04.setBounds(476, 115, 86, 20);
		panel.add(textField_04);
		textField_04.setColumns(10);
		
		textField_05 = new JTextField();
		textField_05.setBounds(582, 115, 86, 20);
		panel.add(textField_05);
		textField_05.setColumns(10);
		
		textField_06 = new JTextField();
		textField_06.setBounds(700, 115, 86, 20);
		panel.add(textField_06);
		textField_06.setColumns(10);
		
		textField_07 = new JTextField();
		textField_07.setBounds(812, 115, 86, 20);
		panel.add(textField_07);
		textField_07.setColumns(10);
		
		queue2 = new ArrayList<JTextField>();
		
		queue2.add(0,textField_00);
		queue2.add(1,textField_01);
		queue2.add(2,textField_02);
		queue2.add(3,textField_03);
		queue2.add(4,textField_04);
		queue2.add(5,textField_05);
		queue2.add(6,textField_06);
		queue2.add(7,textField_07);
		
		textField_10 = new JTextField();
		textField_10.setBounds(41, 168, 86, 20);
		panel.add(textField_10);
		textField_10.setColumns(10);
		
		textField_11 = new JTextField();
		textField_11.setBounds(152, 168, 86, 20);
		panel.add(textField_11);
		textField_11.setColumns(10);
		
		textField_12 = new JTextField();
		textField_12.setBounds(259, 168, 86, 20);
		panel.add(textField_12);
		textField_12.setColumns(10);
		
		textField_13 = new JTextField();
		textField_13.setBounds(368, 168, 86, 20);
		panel.add(textField_13);
		textField_13.setColumns(10);
		
		textField_14 = new JTextField();
		textField_14.setBounds(476, 168, 86, 20);
		panel.add(textField_14);
		textField_14.setColumns(10);
		
		textField_15 = new JTextField();
		textField_15.setBounds(582, 168, 86, 20);
		panel.add(textField_15);
		textField_15.setColumns(10);
		
		textField_16 = new JTextField();
		textField_16.setBounds(700, 168, 86, 20);
		panel.add(textField_16);
		textField_16.setColumns(10);
		
		textField_17 = new JTextField();
		textField_17.setBounds(812, 168, 86, 20);
		panel.add(textField_17);
		textField_17.setColumns(10);
		
		queue3 = new ArrayList<JTextField>();
		
		queue3.add(0,textField_10);
		queue3.add(1,textField_11);
		queue3.add(2,textField_12);
		queue3.add(3,textField_13);
		queue3.add(4,textField_14);
		queue3.add(5,textField_15);
		queue3.add(6,textField_16);
		queue3.add(7,textField_17);
		
		textField_20 = new JTextField();
		textField_20.setBounds(41, 221, 86, 20);
		panel.add(textField_20);
		textField_20.setColumns(10);
		
		textField_21 = new JTextField();
		textField_21.setBounds(152, 221, 86, 20);
		panel.add(textField_21);
		textField_21.setColumns(10);
		
		textField_22 = new JTextField();
		textField_22.setBounds(259, 221, 86, 20);
		panel.add(textField_22);
		textField_22.setColumns(10);
		
		textField_23 = new JTextField();
		textField_23.setBounds(368, 221, 86, 20);
		panel.add(textField_23);
		textField_23.setColumns(10);
		
		textField_24 = new JTextField();
		textField_24.setBounds(476, 221, 86, 20);
		panel.add(textField_24);
		textField_24.setColumns(10);
		
		textField_25 = new JTextField();
		textField_25.setBounds(582, 221, 86, 20);
		panel.add(textField_25);
		textField_25.setColumns(10);
		
		textField_26 = new JTextField();
		textField_26.setBounds(700, 221, 86, 20);
		panel.add(textField_26);
		textField_26.setColumns(10);
		
		textField_27 = new JTextField();
		textField_27.setBounds(812, 221, 86, 20);
		panel.add(textField_27);
		textField_27.setColumns(10);
		
		textField_30 = new JTextField();
		textField_30.setBounds(41, 272, 86, 20);
		panel.add(textField_30);
		textField_30.setColumns(10);
		
		textField_31 = new JTextField();
		textField_31.setBounds(152, 272, 86, 20);
		panel.add(textField_31);
		textField_31.setColumns(10);
		
		textField_32 = new JTextField();
		textField_32.setBounds(259, 272, 86, 20);
		panel.add(textField_32);
		textField_32.setColumns(10);
		
		textField_33 = new JTextField();
		textField_33.setBounds(368, 272, 86, 20);
		panel.add(textField_33);
		textField_33.setColumns(10);
		
		textField_34 = new JTextField();
		textField_34.setBounds(476, 272, 86, 20);
		panel.add(textField_34);
		textField_34.setColumns(10);
		
		textField_35 = new JTextField();
		textField_35.setBounds(582, 272, 86, 20);
		panel.add(textField_35);
		textField_35.setColumns(10);
		
		textField_36 = new JTextField();
		textField_36.setBounds(700, 272, 86, 20);
		panel.add(textField_36);
		textField_36.setColumns(10);
		
		textField_37 = new JTextField();
		textField_37.setBounds(812, 272, 86, 20);
		panel.add(textField_37);
		textField_37.setColumns(10);
		
		queue4 = new ArrayList<JTextField>();
		
		queue4.add(0,textField_20);
		queue4.add(1,textField_21);
		queue4.add(2,textField_22);
		queue4.add(3,textField_23);
		queue4.add(4,textField_24);
		queue4.add(5,textField_25);
		queue4.add(6,textField_26);
		queue4.add(7,textField_27);
		
		queue5 = new ArrayList<JTextField>();
		
		queue5.add(0,textField_30);
		queue5.add(1,textField_31);
		queue5.add(2,textField_32);
		queue5.add(3,textField_33);
		queue5.add(4,textField_34);
		queue5.add(5,textField_35);
		queue5.add(6,textField_36);
		queue5.add(7,textField_37);
		queue.add(0,queue1);
		queue.add(1,queue2);
		queue.add(2,queue3);
		queue.add(3,queue4);
		queue.add(4,queue5);
	}

	public void displayData(Task[] tasks,int index) 
	{
		int i = 0;
		for (Task t : tasks) {
			if (i<=7)
			{
				queue.get(index).get(i).setText(Integer.toString(t.arrivalTime));
			}
			i++;
		}
	}
	
	public void displayAll(Scheduler s, int queueNr)
	{
		for (int i = 1; i<=queueNr;i++)
		{
			int k = 0;
			for (Task t : s.getTasks(i))
			{
				if (k<=7)
				{
					queue.get(i-1).get(k).setText(Integer.toString(t.arrivalTime));
				}
				k++;
			}
		}
	}
}
