package PT2016.Second.Second.View;

import java.awt.BorderLayout;
import PT2016.Second.Second.Engine.*;
import PT2016.Second.Second.Engine.Simulator;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class StartFrame extends JFrame {

	private JPanel contentPane;
	private JTextField minArrivalInterval;
	private JTextField maxArrivalInterval;
	private JTextField minServiceTime;
	private JTextField maxServiceTime;
	private JTextField nrOfQueues;
	private JTextField duration;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					StartFrame frame = new StartFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public StartFrame() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 453, 374);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblMinArrivalInterval = new JLabel("Min arrival interval");
		lblMinArrivalInterval.setBounds(45, 39, 127, 14);
		contentPane.add(lblMinArrivalInterval);
		
		minArrivalInterval = new JTextField();
		minArrivalInterval.setBounds(215, 36, 86, 20);
		contentPane.add(minArrivalInterval);
		minArrivalInterval.setColumns(10);
		
		JLabel lblMaxArrivalInterval = new JLabel("Max arrival interval");
		lblMaxArrivalInterval.setBounds(45, 83, 127, 14);
		contentPane.add(lblMaxArrivalInterval);
		
		maxArrivalInterval = new JTextField();
		maxArrivalInterval.setBounds(215, 80, 86, 20);
		contentPane.add(maxArrivalInterval);
		maxArrivalInterval.setColumns(10);
		
		JLabel lblMinServiceTime = new JLabel("Min service time");
		lblMinServiceTime.setBounds(45, 126, 127, 14);
		contentPane.add(lblMinServiceTime);
		
		minServiceTime = new JTextField();
		minServiceTime.setBounds(215, 123, 86, 20);
		contentPane.add(minServiceTime);
		minServiceTime.setColumns(10);
		
		JLabel lblMaxServiceTime = new JLabel("Max service time");
		lblMaxServiceTime.setBounds(45, 169, 127, 14);
		contentPane.add(lblMaxServiceTime);
		
		maxServiceTime = new JTextField();
		maxServiceTime.setBounds(215, 166, 86, 20);
		contentPane.add(maxServiceTime);
		maxServiceTime.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("Nr. of queues");
		lblNewLabel.setBounds(45, 212, 127, 14);
		contentPane.add(lblNewLabel);
		
		nrOfQueues = new JTextField();
		nrOfQueues.setBounds(215, 209, 86, 20);
		contentPane.add(nrOfQueues);
		nrOfQueues.setColumns(10);
		
		JLabel lblNewLabel_1 = new JLabel("Simulation duration");
		lblNewLabel_1.setBounds(45, 251, 127, 14);
		contentPane.add(lblNewLabel_1);
		
		duration = new JTextField();
		duration.setBounds(215, 248, 86, 20);
		contentPane.add(duration);
		duration.setColumns(10);
		
		JButton btnSubmit = new JButton("Submit");
		btnSubmit.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {

				setVisible(false);

				Data data = new Data();
				data.setDuration(Integer.parseInt(duration.getText()));
				data.setMinArrivalTime(Integer.parseInt(minArrivalInterval.getText()));
				data.setMaxArrivalTime(Integer.parseInt(maxArrivalInterval.getText()));
				data.setMinServiceTime(Integer.parseInt(minServiceTime.getText()));
				data.setMaxServiceTime(Integer.parseInt(maxServiceTime.getText()));
				data.setNrOfQueues(Integer.parseInt(nrOfQueues.getText()));
				data.writeData();
				String[] l = {};
				Simulator.main(l);
			}
		});
		btnSubmit.setBounds(199, 302, 89, 23);
		contentPane.add(btnSubmit);
	}
}
