package PT2016.Second.Second.Engine;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

public class Server implements Runnable{

	AtomicInteger waitingTime = new AtomicInteger(0);
	//BlockingQueue 
	BlockingQueue<Task> bq = new LinkedBlockingQueue<>();
	
	@Override
	public void run()
	{
		while(true)
		{
			
			try(FileWriter fw = new FileWriter("log.txt", true);
				    BufferedWriter bw = new BufferedWriter(fw);
				    PrintWriter out = new PrintWriter(bw))
				{
				try {
					Task t = bq.take();
					out.println("Customer with arrival time: " + Integer.toString(t.getArrivalTime()) + " start.");
					out.println("	in queue: " + Integer.toString(t.getQueue()));
					out.println("	process time: " + Integer.toString(t.getProcessTime()));
					out.println("	waiting time: " + waitingTime.toString());

					Thread.sleep(t.getProcessTime()*1000);

					waitingTime.addAndGet((-1)*t.getProcessTime());
					out.println("Customer with arrival time: " + Integer.toString(t.getArrivalTime()) + " finish.");
					out.println("	waiting time: " + waitingTime.toString());
					out.println("");
				} catch (InterruptedException e) {
				// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} catch (IOException e) {
					//exception handling left as an exercise for the reader
			}	
		}
	}
	
	public Server()
	{
		
	}
	
	public void addTask(Task t)
	{
		bq.add(t);
		waitingTime.addAndGet(t.getProcessTime());
	}
	
	public Task[] getTasks()
	{
		Task[] tasks = new Task[bq.size()];
		bq.toArray(tasks);
		return tasks;
	}
}
