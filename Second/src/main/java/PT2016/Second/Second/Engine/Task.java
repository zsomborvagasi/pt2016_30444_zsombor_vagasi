package PT2016.Second.Second.Engine;

public class Task {

	public int arrivalTime;
	public int processTime;
	public int queue;
	
	public Task()
	{
		
	}

	public Task(int arrivalTime, int processTime,int queue) {
		super();
		this.arrivalTime = arrivalTime;
		this.processTime = processTime;
		this.queue = queue;
	}

	public int getArrivalTime() {
		return arrivalTime;
	}

	public void setArrivalTime(int arrivalTime) {
		this.arrivalTime = arrivalTime;
	}

	public int getProcessTime() {
		return processTime;
	}

	public void setProcessTime(int processTime) {
		this.processTime = processTime;
	}
	
	public int getQueue() {
		return queue;
	}
	
	public void setQueue(int queue) {
		this.queue = queue;
	}
}
