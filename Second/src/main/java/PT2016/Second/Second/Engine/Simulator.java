

package PT2016.Second.Second.Engine;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.Random;

import PT2016.Second.Second.View.*;

public class Simulator implements Runnable{
	
	int finishTime;
	int minProcessTime;
	int maxProcessTime;
	int minArrivalTime;
	int maxArrivalTime;
	int nrOfQueues;
	static Scheduler s;
	static MainFrame frame;
	Data localData;
	PrintWriter writer ;
	int currentTime;

	@Override
	public void run()
	{
		currentTime = 0;
		Data data = new Data();
		Integer[] list = data.getData();
		finishTime = list[5];
		nrOfQueues = list[4];
		minProcessTime = list[2];
		maxProcessTime = list[3];
		minArrivalTime = list[0];
		maxArrivalTime = list[1];
		try {
			writer = new PrintWriter("log.txt", "UTF-8");
			while (currentTime < finishTime)
			{
				currentTime++;
				
				//writer.println("Current time: " + Integer.toString(currentTime));
				
		        Random rand = new Random();
				int processTime = rand.nextInt(maxProcessTime-minProcessTime + 1) + minProcessTime;
				
				//writer.println("	process time: " + Integer.toString(processTime));
				
				int queueNr = rand.nextInt(nrOfQueues) + 1;
				Task t = new Task(currentTime,processTime,queueNr);
				int toBeDisplayed = 0;

				//frame.displayData(s.getTasks(queueNr),queueNr);
				//writer.println("	queue number: " + Integer.toString(queueNr));
				//writer.println("");
				if ((currentTime >= minArrivalTime) && (currentTime <= maxArrivalTime))
				{
					toBeDisplayed = s.dispatchTaskOnServer(t);
				}
				try {
					Thread.sleep(200);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				frame.displayData(s.getTasks(toBeDisplayed),toBeDisplayed);
				//frame.displayAll(s, nrOfQueues);
			}
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static  void main(String[] args)
	{
		Thread testThread = new Thread(new Simulator());
		testThread.start();
	}
	
	
	public Simulator()
	{
		s = new Scheduler();
		frame = new MainFrame();
	}
}