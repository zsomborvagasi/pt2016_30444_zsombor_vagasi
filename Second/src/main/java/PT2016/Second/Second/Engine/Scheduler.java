package PT2016.Second.Second.Engine;

import java.util.ArrayList;

public class Scheduler {

	public ArrayList<Server> threadList;
	
	public int dispatchTaskOnServer(Task t)
	{
		int minWaitTime = 1000;
		int indexServer = 0;
		int index = 0;
		for (Server s : threadList)
		{
			if (s.waitingTime.get() < minWaitTime)
			{
				minWaitTime = s.waitingTime.get();
				indexServer = index;
			}
			index++;
		}
		threadList.get(indexServer).addTask(t);
		return indexServer;
	}
	
	public Scheduler()
	{
		Data data = new Data();
		Integer[] list = data.getData();
		int nrOfQueues = list[4];
		

		threadList = new ArrayList<Server>();
		for (int i = 0;i< nrOfQueues;i++)
		{
			Server s = new Server();
			threadList.add(s);
			Thread th = new Thread(threadList.get(i));
			th.start();
		}

	}
	
	public Task[] getTasks(int index)
	{
		
		return threadList.get(index).getTasks();
	}
} 
