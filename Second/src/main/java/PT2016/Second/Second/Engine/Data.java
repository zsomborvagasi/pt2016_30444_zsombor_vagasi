package PT2016.Second.Second.Engine;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JOptionPane;

public class Data {
	static public int minArrivalTime;
	static public int maxArrivalTime;
	static public int minServiceTime;
	static public int maxServiceTime;
	static public int nrOfQueues;
	static public int duration;
	
	public int getMinArrivalTime() {
		return minArrivalTime;
	}
	public void setMinArrivalTime(int minArrivalTime) {
		Data.minArrivalTime = minArrivalTime;
	}
	public int getMaxArrivalTime() {
		return maxArrivalTime;
	}
	public void setMaxArrivalTime(int maxArrivalTime) {
		Data.maxArrivalTime = maxArrivalTime;
	}
	public int getMinServiceTime() {
		return minServiceTime;
	}
	public void setMinServiceTime(int minServiceTime) {
		Data.minServiceTime = minServiceTime;
	}
	public int getMaxServiceTime() {
		return maxServiceTime;
	}
	public void setMaxServiceTime(int maxServiceTime) {
		Data.maxServiceTime = maxServiceTime;
	}
	public int getNrOfQueues() {
		return nrOfQueues;
	}
	public void setNrOfQueues(int nrOfQueues) {
		Data.nrOfQueues = nrOfQueues;
	}
	public int getDuration() {
		return Data.duration;
	}
	public void setDuration(int duration) {
		Data.duration = duration;
	}
	
	public Data()
	{
		//JOptionPane.showMessageDialog(null, Integer.toString(getDuration()), "Warning!", JOptionPane.INFORMATION_MESSAGE);
	}
	
	public void writeData()
	{
		PrintWriter writer;
		try {
			writer = new PrintWriter("config.txt", "UTF-8");
			writer.println(Integer.toString(getMinArrivalTime()));
			writer.println(Integer.toString(getMaxArrivalTime()));
			writer.println(Integer.toString(getMinServiceTime()));
			writer.println(Integer.toString(getMaxServiceTime()));
			writer.println(Integer.toString(getNrOfQueues()));
			writer.println(Integer.toString(getDuration()));
			writer.println("");
			writer.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	 
	public Integer[] getData()
	{
		Integer[] list = new Integer[6];
		try {
			BufferedReader in = new BufferedReader(new FileReader("config.txt"));
			try {
				String line = in.readLine();
				int index  =0;
				while (!line.isEmpty())
				{
					list[index] = Integer.parseInt(line);
					line = in.readLine();
					index ++;
				}
				return list;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
