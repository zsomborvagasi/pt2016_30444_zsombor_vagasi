package View;

import Engine.*;
import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.table.DefaultTableModel;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class MainWindow extends JFrame {

	private JPanel contentPane;
	static DictionaryFacade facade;
	private JTable table;
	private JTextField textField;
	private JTextField textField_1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainWindow frame = new MainWindow();
					facade = new DictionaryFacade();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MainWindow() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 700, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(371, 52, 273, 138);
		contentPane.add(scrollPane);
		
		table = new JTable();
		table.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"Word", "Definition"
			}
		));
		scrollPane.setViewportView(table);
		
		JLabel lblNewLabel = new JLabel("Word");
		lblNewLabel.setBounds(47, 39, 62, 14);
		contentPane.add(lblNewLabel);
		
		textField = new JTextField();
		textField.setBounds(138, 36, 86, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JLabel lblNewLabel_1 = new JLabel("Definition");
		lblNewLabel_1.setBounds(47, 80, 62, 14);
		contentPane.add(lblNewLabel_1);
		
		textField_1 = new JTextField();
		textField_1.setBounds(138, 77, 86, 20);
		contentPane.add(textField_1);
		textField_1.setColumns(10);
		
		JButton btnNewButton = new JButton("Add");
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				String word = textField.getText();
				String definition = textField_1.getText();
				facade.addNotionFacade(word, definition);
			}
		});
		btnNewButton.setBounds(80, 147, 89, 23);
		contentPane.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Remove");
		btnNewButton_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				String word = textField.getText();
				String definition = textField_1.getText();
				facade.deleteNotionFacade(word,definition);
			}
		});
		btnNewButton_1.setBounds(80, 186, 89, 23);
		contentPane.add(btnNewButton_1);
		
		JButton btnNewButton_2 = new JButton("Change");
		btnNewButton_2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				String word = textField.getText();
				String definition = textField_1.getText();
				facade.changeNotionFacade(word, definition);
			}
		});
		btnNewButton_2.setBounds(80, 230, 89, 23);
		contentPane.add(btnNewButton_2);
		
		JButton btnNewButton_3 = new JButton("Search");
		btnNewButton_3.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				table.removeAll();
				table.repaint();
				String word = textField.getText();
				String definition = textField_1.getText();
				String searchedWord = "";
				String searchedDefinition = "";
				
				Document doc = facade.getXml();

				searchedWord = getRegex(word);
				searchedDefinition = getRegex(definition);
				
				Node dictionary = doc.getFirstChild();

				// loop the staff child node
				NodeList list = dictionary.getChildNodes();
				DefaultTableModel model = (DefaultTableModel) table.getModel();
				model.setRowCount(0);
				table.revalidate();

				for (int i = 0; i < list.getLength(); i++) {
					Node node = list.item(i);
					if (node.getNodeName().matches(searchedWord) && node.getAttributes().getNamedItem("definition").getNodeValue().matches(searchedDefinition))
					{
						model.addRow(new Object[]{node.getNodeName(),node.getAttributes().getNamedItem("definition").getNodeValue()});
					}
				}
			}
		});
		btnNewButton_3.setBounds(80, 323, 89, 23);
		contentPane.add(btnNewButton_3);
		
		JButton btnShow = new JButton("Show");
		btnShow.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				Document doc = facade.getXml();
				Node dictionary = doc.getFirstChild();

				// loop the staff child node
				NodeList list = dictionary.getChildNodes();
				DefaultTableModel model = (DefaultTableModel) table.getModel();
				model.setRowCount(0);
				table.revalidate();

				for (int i = 0; i < list.getLength(); i++) {
					Node node = list.item(i);
					model.addRow(new Object[]{node.getNodeName(),node.getAttributes().getNamedItem("definition").getNodeValue()});
				}
			}
		});
		btnShow.setBounds(80, 278, 89, 23);
		contentPane.add(btnShow);
	}
	String getRegex (String word)
	{
		if (word.equals(""))
		{
			return "[a-zA-z\\s]+";
		}
		word = word.replaceAll("\\*", "[a-zA-z\\s]+");
		word = word.replaceAll("\\?", "[a-zA-z\\s]?");
		//JOptionPane.showMessageDialog(null, word, "Warning!", JOptionPane.INFORMATION_MESSAGE);

		return word;
	}
}
