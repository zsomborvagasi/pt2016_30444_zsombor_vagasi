package Engine;

public class Notion {
	public String notion;
	public String definition;
	public String getNotion() {
		return notion;
	}
	public void setNotion(String notion) {
		this.notion = notion;
	}
	public String getDefinition() {
		return definition;
	}
	public void setDefinition(String definition) {
		this.definition = definition;
	}
	public Notion(String notion, String definition) {
		super();
		this.notion = notion;
		this.definition = definition;
	}
	
	
}
