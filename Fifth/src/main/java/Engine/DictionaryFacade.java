package Engine;

import org.w3c.dom.Document;

public class DictionaryFacade {
	public DictionaryImplementation dictionary;
	
	public DictionaryFacade()
	{
		dictionary = new DictionaryImplementation();
	}
	
	public void addNotionFacade(String word,String definition)
	{
		Notion notion = new Notion(word,definition);
		dictionary.addNotion(notion);
	}
	
	public void deleteNotionFacade(String word,String definition)
	{
		Notion notion = new Notion(word,definition);
		dictionary.deleteNotion(notion);
	}
	
	public void changeNotionFacade(String word,String definition)
	{
		Notion notion = new Notion(word,definition);
		dictionary.changeNotion(notion);
	}
	
	public Document getXml()
	{
		return dictionary.getXml();
	}
}
