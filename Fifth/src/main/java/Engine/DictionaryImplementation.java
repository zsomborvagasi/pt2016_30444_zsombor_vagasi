package Engine;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class DictionaryImplementation implements DictionaryInterface{
	public void addNotion(Notion notion)
	{
		try {
			String filepath = "dictionary.xml";
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			Document doc = docBuilder.parse(filepath);

			// Get the root element
			Node dictionary = doc.getFirstChild();

			// append a new node to dictionary
			Element def = doc.createElement(notion.notion);
			def.setAttribute("definition", notion.definition);
			dictionary.appendChild(def);


			// write the content into xml file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(filepath));
			transformer.transform(source, result);


		   } catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		   } catch (TransformerException tfe) {
			tfe.printStackTrace();
		   } catch (IOException ioe) {
			ioe.printStackTrace();
		   } catch (SAXException sae) {
			sae.printStackTrace();
		   }
	}
	
	public void deleteNotion(Notion notion)
	{
		try {
			String filepath = "dictionary.xml";
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			Document doc = docBuilder.parse(filepath);

			// Get the root element
			Node dictionary = doc.getFirstChild();

			// loop the staff child node
			NodeList list = dictionary.getChildNodes();

			for (int i = 0; i < list.getLength(); i++) {
				
	                   Node node = list.item(i);

			   // get the salary element, and update the value
			   if (notion.notion.equals(node.getNodeName())) {
				dictionary.removeChild(node);
			   }
			}


			// write the content into xml file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(filepath));
			transformer.transform(source, result);


		   } catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		   } catch (TransformerException tfe) {
			tfe.printStackTrace();
		   } catch (IOException ioe) {
			ioe.printStackTrace();
		   } catch (SAXException sae) {
			sae.printStackTrace();
		   }
	}
	
	public void changeNotion(Notion notion)
	{
		try {
			String filepath = "dictionary.xml";
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			Document doc = docBuilder.parse(filepath);

			// Get the root element
			Node dictionary = doc.getFirstChild();

			// loop the staff child node
			NodeList list = dictionary.getChildNodes();

			for (int i = 0; i < list.getLength(); i++) {
				
	           Node node = list.item(i);
			   if (notion.notion.equals(node.getNodeName())) {
				   node.getAttributes().getNamedItem("definition").setTextContent(notion.definition);
			   }
			}


			// write the content into xml file
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(filepath));
			transformer.transform(source, result);


		   } catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		   } catch (TransformerException tfe) {
			tfe.printStackTrace();
		   } catch (IOException ioe) {
			ioe.printStackTrace();
		   } catch (SAXException sae) {
			sae.printStackTrace();
		   }
	}
	
	public DictionaryImplementation()
	{
		
	}
	
	public Notion searchNotion (Notion notion)
	{
		try {
			String filepath = "dictionary.xml";
			DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			Document doc = docBuilder.parse(filepath);

			// Get the root element
			Node dictionary = doc.getFirstChild();

			// loop the staff child node
			NodeList list = dictionary.getChildNodes();

			for (int i = 0; i < list.getLength(); i++) {
				
	           Node node = list.item(i);
			   if (notion.notion.equals(node.getNodeName())) {
				   notion.setNotion(node.getAttributes().getNamedItem("definition").getTextContent());
			   }
			}
		   } catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		   } catch (IOException ioe) {
			ioe.printStackTrace();
		   } catch (SAXException sae) {
			sae.printStackTrace();
		   }
		return notion;
	}
	
	public void createXml()
	{
		try {
			File f = new File("directory.xml");
			if(!f.exists() && !f.isDirectory()) { 
				DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
				DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

				// root elements
				Document doc = docBuilder.newDocument();
				Element rootElement = doc.createElement("dictionary");
				doc.appendChild(rootElement);

				// write the content into xml file
				TransformerFactory transformerFactory = TransformerFactory.newInstance();
				Transformer transformer = transformerFactory.newTransformer();
				DOMSource source = new DOMSource(doc);
				StreamResult result = new StreamResult(new File("dictionary.xml"));

				transformer.transform(source, result);
			}
			} catch (ParserConfigurationException pce) {
				pce.printStackTrace();
			} catch (TransformerException tfe) {
				tfe.printStackTrace();
			}
	}
	
	public Document getXml()
	{
		try {
			String filepath = "dictionary.xml";
			File f = new File("directory.xml");
			if(!f.exists() && !f.isDirectory()) { 
				DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
				DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
				Document doc = docBuilder.parse(filepath);
				return doc;
			}
		   } catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		   } catch (IOException ioe) {
			ioe.printStackTrace();
		   } catch (SAXException sae) {
			sae.printStackTrace();
		   }
		return null;
	}
}
