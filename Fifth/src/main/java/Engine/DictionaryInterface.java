package Engine;

import org.w3c.dom.Document;

public interface DictionaryInterface {
	public void addNotion(Notion notion);
	public void deleteNotion(Notion notion);
	public void changeNotion(Notion notion);
	public void createXml();
	public Notion searchNotion(Notion notion);
	public Document getXml();
}
