package View;

import Engine.*;
import java.awt.BorderLayout;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class MainWindow extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private JTable table_1;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	static Bank bank;
	private JTextField textField_5;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainWindow frame = new MainWindow();
					frame.setVisible(true);
					bank = new Bank();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MainWindow() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 700, 600);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(45, 29, 262, 113);
		contentPane.add(scrollPane);
		
		table = new JTable();
		table.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"Person", "Account"
			}
		));
		scrollPane.setViewportView(table);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(356, 29, 276, 113);
		contentPane.add(scrollPane_1);
		
		table_1 = new JTable();
		table_1.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"Person", "Account"
			}
		));
		scrollPane_1.setViewportView(table_1);
		
		textField = new JTextField();
		textField.setBounds(161, 332, 86, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setBounds(161, 387, 86, 20);
		contentPane.add(textField_1);
		textField_1.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("Name");
		lblNewLabel.setBounds(63, 335, 46, 14);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("ID");
		lblNewLabel_1.setBounds(63, 390, 46, 14);
		contentPane.add(lblNewLabel_1);
		
		JButton btnNewButton = new JButton("Remove");
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				String name = "";
				String id = "";
				
				name = textField.getText();
				id = textField_1.getText();
				Person person = new Person(Integer.parseInt(id),name);
				
				if (bank.checkPerson(person))
				{
					
					bank.removePerson(person);
					DefaultTableModel model = (DefaultTableModel) table.getModel();
					for (int i = 0; i < table.getRowCount();i++)
					{
						if (model.getValueAt(i, 0).equals(name + " - "+id))
						{
							model.removeRow(i);
						}
					}
					
					DefaultTableModel model1 = (DefaultTableModel) table_1.getModel();
					for (int i = 0; i < table_1.getRowCount();i++)
					{
						if (model1.getValueAt(i, 0).equals(name + " - "+id))
						{
							model1.removeRow(i);
						}
					}
				}
				else
				{
					JOptionPane.showMessageDialog(null, "Person with same ID already exists.", "Warning!", JOptionPane.INFORMATION_MESSAGE);
				}
			}
		});
		btnNewButton.setBounds(63, 449, 89, 23);
		contentPane.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Add");
		btnNewButton_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				String name = "";
				String id = "";
				
				name = textField.getText();
				id = textField_1.getText();
				Person person = new Person(Integer.parseInt(id),name);
				
				if (!bank.checkPerson(person))
				{
					
					bank.addPerson(person);
					JOptionPane.showMessageDialog(null, "Person created for savings and spending!", "Warning!", JOptionPane.INFORMATION_MESSAGE);

				}
				else
				{
					JOptionPane.showMessageDialog(null, "Person with same ID already exists.", "Warning!", JOptionPane.INFORMATION_MESSAGE);
				}
			}
		});
		btnNewButton_1.setBounds(161, 449, 89, 23);
		contentPane.add(btnNewButton_1);
		
		textField_2 = new JTextField();
		textField_2.setBounds(489, 332, 86, 20);
		contentPane.add(textField_2);
		textField_2.setColumns(10);
		
		JLabel lblNewLabel_2 = new JLabel("Type");
		lblNewLabel_2.setBounds(356, 335, 83, 14);
		contentPane.add(lblNewLabel_2);
		
		textField_3 = new JTextField();
		textField_3.setBounds(489, 387, 86, 20);
		contentPane.add(textField_3);
		textField_3.setColumns(10);
		
		JLabel lblNewLabel_3 = new JLabel("ID");
		lblNewLabel_3.setBounds(356, 390, 83, 14);
		contentPane.add(lblNewLabel_3);
		
		textField_4 = new JTextField();
		textField_4.setBounds(489, 450, 86, 20);
		contentPane.add(textField_4);
		textField_4.setColumns(10);
		
		JLabel lblNewLabel_4 = new JLabel("Amount");
		lblNewLabel_4.setBounds(356, 453, 83, 14);
		contentPane.add(lblNewLabel_4);
		
		JButton btnDeposit = new JButton("Deposit");
		btnDeposit.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				String type = "";
				String personId = "";
				String accId = "";
				int amount = 0;

				
				accId = textField_5.getText();
				type = textField_2.getText();
				personId = textField_3.getText();
				amount = Integer.parseInt(textField_4.getText());
				int index = personId.indexOf("-");
				Person person = new Person(Integer.parseInt(personId.substring(index + 2)),personId.substring(0, index - 1));
				if (type.equals("savings"))
				{
					//JOptionPane.showMessageDialog(null, "1", "Warning!", JOptionPane.INFORMATION_MESSAGE);
					if (bank.savingsAccountList.containsKey(person))
					{
						bank.depositMoneySavings(person, Integer.parseInt(accId), (double)amount);
						DefaultTableModel model = (DefaultTableModel) table.getModel();
						//JOptionPane.showMessageDialog(null, "2", "Warning!", JOptionPane.INFORMATION_MESSAGE);
						for (int i = 0; i < table.getRowCount();i++)
						{
							//JOptionPane.showMessageDialog(null, "3", "Warning!", JOptionPane.INFORMATION_MESSAGE);
							if (model.getValueAt(i, 0).equals(personId) && model.getValueAt(i, 1).toString().contains(" - " + accId))
							{
								//JOptionPane.showMessageDialog(null, "4", "Warning!", JOptionPane.INFORMATION_MESSAGE);
								for (SavingsAccount localAcc : bank.savingsAccountList.get(person))
								{
									
									//JOptionPane.showMessageDialog(null, "5", "Warning!", JOptionPane.INFORMATION_MESSAGE);
									if (localAcc.getAccId() == Integer.parseInt(accId))
									{
										//JOptionPane.showMessageDialog(null, "6", "Warning!", JOptionPane.INFORMATION_MESSAGE);
										model.setValueAt((int)localAcc.getMoney() + " - " + accId, i, 1);
									}
								}
							}
						}
					}
				}
				
				if (type.equals("spending"))
				{
					if (bank.spendingAccountList.containsKey(person))
					{
						bank.depositMoneySpending(person, Integer.parseInt(accId), (double)amount);
						DefaultTableModel model = (DefaultTableModel) table_1.getModel();
						for (int i = 0; i < table_1.getRowCount();i++)
						{
							if (model.getValueAt(i, 0).equals(personId) && model.getValueAt(i, 1).toString().contains(" - " + accId))
							{
								
								for (SpendingAccount localAcc : bank.spendingAccountList.get(person))
								{
									
									if (localAcc.getAccId() == Integer.parseInt(accId))
									{
										model.setValueAt((int)localAcc.getMoney()+ " - " + accId, i, 1);
									}
								}
							}
						}
					}
				}
			}
		});
		btnDeposit.setBounds(350, 507, 89, 23);
		contentPane.add(btnDeposit);
		
		JButton btnAdd = new JButton("Add");
		btnAdd.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				String type = "";
				String personId = "";
				String accId = "";
				int amount = 0;

				
				accId = textField_5.getText();
				type = textField_2.getText();
				personId = textField_3.getText();
				amount = Integer.parseInt(textField_4.getText());
				int index = personId.indexOf("-");
				Person person = new Person(Integer.parseInt(personId.substring(index + 2)),personId.substring(0, index - 1));
				
				if (bank.checkPerson(person))
				{
					
					if (type.equals("savings"))
					{
						SavingsAccount acc = new SavingsAccount(Integer.parseInt(accId),(double)amount);
						
						if (!bank.savingsAccountList.get(person).contains(acc))
						{
							bank.addSavingsToPerson(person,acc );
							DefaultTableModel model = (DefaultTableModel) table.getModel();
							model.addRow(new Object[]{personId,Integer.toString(amount) + " - " + accId});
						}
						else
						{
							JOptionPane.showMessageDialog(null, "Account with same ID exists.", "Warning!", JOptionPane.INFORMATION_MESSAGE);
						}

					}
					else if (type.equals("spending"))
					{
						SpendingAccount acc = new SpendingAccount(Integer.parseInt(accId),(double)amount);
						if (!bank.spendingAccountList.get(person).contains(acc))
						{
							bank.addSpendingToPerson(person,acc );
							DefaultTableModel model = (DefaultTableModel) table_1.getModel();
							model.addRow(new Object[]{personId,Integer.toString(amount) + " - " + accId});
						}
						else
						{
							JOptionPane.showMessageDialog(null, "Account with same ID exists.", "Warning!", JOptionPane.INFORMATION_MESSAGE);
						}

					}
				}
				else
				{
					JOptionPane.showMessageDialog(null, "Person with ID doesn't exists.", "Warning!", JOptionPane.INFORMATION_MESSAGE);
				}
			}
		});
		btnAdd.setBounds(454, 507, 89, 23);
		contentPane.add(btnAdd);
		
		JButton btnRemove = new JButton("Remove");
		btnRemove.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				String type = "";
				String personId = "";
				String accId = "";
				int amount = 0;

				
				accId = textField_5.getText();
				type = textField_2.getText();
				personId = textField_3.getText();
				amount = Integer.parseInt(textField_4.getText());
				int index = personId.indexOf("-");
				Person person = new Person(Integer.parseInt(personId.substring(index + 2)),personId.substring(0, index - 1));
				
				if (bank.checkPerson(person))
				{
					
					if (type.equals("savings"))
					{
						SavingsAccount acc = new SavingsAccount(Integer.parseInt(accId),(double)amount);
						bank.removeSavingsToPerson(person,acc );
						DefaultTableModel model = (DefaultTableModel) table.getModel();
						for (int i = 0; i < table.getRowCount();i++)
						{
							if (model.getValueAt(i, 0).equals(personId) && model.getValueAt(i, 1).equals(Integer.toString(amount) + " - " + accId))
							{
								model.removeRow(i);
							}
						}
					}
					else if (type.equals("spending"))
					{
						SpendingAccount acc = new SpendingAccount(Integer.parseInt(accId),(double)amount);
						bank.removeSpendingToPerson(person,acc );
						DefaultTableModel model = (DefaultTableModel) table_1.getModel();
						for (int i = 0; i < table_1.getRowCount();i++)
						{
							if (model.getValueAt(i, 0).equals(personId) && model.getValueAt(i, 1).equals(Integer.toString(amount) + " - " + accId))
							{
								model.removeRow(i);
							}
						}
					}
				}
				else
				{
					JOptionPane.showMessageDialog(null, "Person with ID doesn't exists.", "Warning!", JOptionPane.INFORMATION_MESSAGE);
				}
			}
		});
		btnRemove.setBounds(564, 507, 89, 23);
		contentPane.add(btnRemove);
		
		textField_5 = new JTextField();
		textField_5.setBounds(489, 284, 86, 20);
		contentPane.add(textField_5);
		textField_5.setColumns(10);
		
		JLabel lblAccountId = new JLabel("Account ID");
		lblAccountId.setBounds(356, 287, 83, 14);
		contentPane.add(lblAccountId);
	}
}
