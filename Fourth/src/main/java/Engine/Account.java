package Engine;

import java.util.Observable;

public class Account extends Observable {
	protected int accId;
	protected double money;

	public Account(int accId, double money) {
		super();
		this.accId = accId;
		this.money = money;
	}

	@Override
	public String toString() {
		return "Account [accId=" + accId + ", money=" + money + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + accId;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Account other = (Account) obj;
		if (accId != other.accId)
			return false;
		return true;
	}

	public int getAccId() {
		return accId;
	}

	public void setAccId(int accId) {
		this.accId = accId;
	}

	public double getMoney() {
		return money;
	}

	public void setMoney(double money) {
		this.money = money;
	}

	public void addMoney(double sum) {
		money = money + sum;
		setChanged();
		notifyObservers(sum);
	}
}
