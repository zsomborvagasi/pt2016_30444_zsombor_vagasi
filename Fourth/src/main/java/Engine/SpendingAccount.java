package Engine;

public class SpendingAccount extends Account{

	public SpendingAccount(int accId, double money) {
		super(accId, money);
		// TODO Auto-generated constructor stub
	}

	public double getMoney()
	{
		return money - 1;
	}
	
	public void addMoney(double sum)
	{
		money = money + sum - 1;
		setChanged();
		notifyObservers(sum - 1);
	}
}
