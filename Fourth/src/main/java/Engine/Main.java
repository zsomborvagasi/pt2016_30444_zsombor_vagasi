package Engine;

public class Main {

	public static void main(String args[])
	{
		Bank bank = new Bank();
		Person p = new Person(1,"John");
		//Person p = null;
		SpendingAccount acc = new SpendingAccount(1,200);
		SavingsAccount acc2 = new SavingsAccount(1,200);
		//Account acc = null;
		bank.addPerson(p);
		bank.addSpendingToPerson(p, acc);
		bank.addSavingsToPerson(p, acc2);
		//bank.depositMoney(300, 1, p);
		System.out.println(bank.toString());
	}
}
