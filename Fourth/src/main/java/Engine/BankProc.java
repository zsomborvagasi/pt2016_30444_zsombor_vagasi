package Engine;

public interface BankProc {

	public void addPerson(Person person);
	public void removePerson(Person person);
	public void addSavingsToPerson(Person person, SavingsAccount account);
	public void removeSavingsToPerson(Person person, SavingsAccount account);
	public void addSpendingToPerson(Person person, SpendingAccount account);
	public void removeSpendingToPerson(Person person, SpendingAccount account);
	public void depositMoneySavings(Person person, int accountID, double sum);
	public void depositMoneySpending(Person person, int accountID, double sum);
}
