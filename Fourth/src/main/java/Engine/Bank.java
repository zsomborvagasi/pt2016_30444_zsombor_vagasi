package Engine;

import java.io.*;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Bank implements BankProc{

	public Map<Person,Set<SavingsAccount>> savingsAccountList;
	public Map<Person,Set<SpendingAccount>> spendingAccountList;
	
	public Bank()
	{
		savingsAccountList = new HashMap<Person,Set<SavingsAccount>>();
		spendingAccountList = new HashMap<Person,Set<SpendingAccount>>();
		/*try
	      {
	         FileInputStream fileIn = new FileInputStream("savings.ser");
	         ObjectInputStream in = new ObjectInputStream(fileIn);
	         savingsAccountList = (Map<Person,Set<SavingsAccount>>) in.readObject();
	         in.close();
	         fileIn.close();
	      }catch(IOException i)
	      {
	         i.printStackTrace();
	         return;
	      }catch(ClassNotFoundException c)
	      {
	         System.out.println("Class not found");
	         c.printStackTrace();
	         return;
	      }
		
		try
	      {
	         FileInputStream fileIn = new FileInputStream("spending.ser");
	         ObjectInputStream in = new ObjectInputStream(fileIn);
	         spendingAccountList = (Map<Person,Set<SpendingAccount>>) in.readObject();
	         in.close();
	         fileIn.close();
	      }catch(IOException i)
	      {
	         i.printStackTrace();
	         return;
	      }catch(ClassNotFoundException c)
	      {
	         System.out.println("Class not found");
	         c.printStackTrace();
	         return;
	      }*/

	}
	
	public void addSpendingToPerson(Person person, SpendingAccount account)
	{
		assert person != null : "The person should not be null!";
		assert account != null : "The account should not be null";
		account.addObserver(person);
		Set<SpendingAccount> localSet;
		localSet = spendingAccountList.get(person);
		if (localSet != null)
		{
			localSet.add(account);
			spendingAccountList.put(person,localSet);
		}
		else
		{
			addPerson(person);
			Set<SpendingAccount> newSet = new HashSet<SpendingAccount>();
			newSet.add(account);
			spendingAccountList.put(person, newSet);
		}
		/*
		try
	      {
	         FileOutputStream fileOut =
	         new FileOutputStream("spending.ser");
	         ObjectOutputStream out = new ObjectOutputStream(fileOut);
	         out.writeObject(spendingAccountList);
	         out.close();
	         fileOut.close();
	         System.out.printf("Serialized data for spending is saved in spending.ser");
	      }catch(IOException i)
	      {
	          i.printStackTrace();
	      }
	      */
		//postcondition
		assert spendingAccountList.get(person) != null : "Account set is null";
	}
	
	public void removeSpendingToPerson(Person person, SpendingAccount account)
	{
		assert person !=null: "The person should not be null!";
		assert account !=null : "The account should nto be null";
		
		account.deleteObserver(person);
		Set<SpendingAccount> localSet;
		localSet = spendingAccountList.get(person);
		if (localSet != null)
		{
			localSet.remove(account);
		}
	}
	
	public void addSavingsToPerson(Person person, SavingsAccount account)
	{
		assert person != null : "The person should not be null!";
		assert account != null : "The account should not be null";
		account.addObserver(person);
		Set<SavingsAccount> localSet;
		localSet = savingsAccountList.get(person);
		if ((localSet != null) && !savingsAccountList.get(person).contains(account) )
		{
			localSet.add(account);
			savingsAccountList.put(person, localSet);
		}
		else
		{
			addPerson(person);
			Set<SavingsAccount> newSet = new HashSet<SavingsAccount>();
			newSet.add(account);
			savingsAccountList.put(person, newSet);
		}
		/*
		try
	      {
	         FileOutputStream fileOut =
	         new FileOutputStream("savings.ser");
	         ObjectOutputStream out = new ObjectOutputStream(fileOut);
	         out.writeObject(savingsAccountList);
	         out.close();
	         fileOut.close();
	         System.out.printf("Serialized data for savings is saved in savings.ser");
	      }catch(IOException i)
	      {
	          i.printStackTrace();
	      }
	      */
		//postcondition
		assert localSet != null : "Account set is null";
	}
	
	public void removeSavingsToPerson(Person person, SavingsAccount account)
	{
		assert person !=null: "The person should not be null!";
		assert account !=null : "The account should not be null";
		
		account.deleteObserver(person);
		Set<SavingsAccount> localSet;
		localSet = savingsAccountList.get(person);
		if (localSet != null)
		{
			localSet.remove(account);
		}
	}
	

	
	public void addPerson(Person person)
	{
		assert person != null : "The person should not be null!";

		if (!savingsAccountList.containsKey(person))
		{
			Set<SavingsAccount> newSet = new HashSet<SavingsAccount>();
			savingsAccountList.put(person, newSet);
		}
		
		if (!spendingAccountList.containsKey(person))
		{
			Set<SpendingAccount> newSet = new HashSet<SpendingAccount>();
			spendingAccountList.put(person, newSet);
		}
		
	}
	
	public void removePerson(Person person)
	{
		assert person !=null : "Person should not be null!";
		
		if (savingsAccountList.containsKey(person))
		{
			savingsAccountList.remove(person);
		}
		
		if (spendingAccountList.containsKey(person))
		{
			spendingAccountList.remove(person);
		}
	}
	
	public void depositMoneySavings(Person person, int accountID, double sum)
	{
		Set<SavingsAccount> localSet = savingsAccountList.get(person);
		if (localSet != null)
		{
			//iterator for Set
			for (SavingsAccount a : localSet)
			{
				if (a.accId == accountID)
				{
					a.addMoney(sum);
				}
			}
		}
	}
	
	public void depositMoneySpending(Person person, int accountID, double sum)
	{
		Set<SpendingAccount> localSet = spendingAccountList.get(person);
		if (localSet != null)
		{
			//iterator for Set
			for (SpendingAccount a : localSet)
			{
				if (a.accId == accountID)
				{
					a.addMoney(sum);
				}
			}
		}
	}
	
	/*
	public void addAccforPerson(Person p, Account assocAcc)
	{
		//precondition
		assert p != null : "The person should not be null!";
		assert assocAcc != null : "The account should not be null";
		assocAcc.addObserver(p);
		Set<Account> localSet;
		localSet = accountList.get(p);
		if (localSet != null)
		{
			localSet.add(assocAcc);
		}
		else
		{
			Set<Account> newSet = new HashSet<Account>();
			newSet.add(assocAcc);
			accountList.put(p, newSet);
		}
		//postcondition
		assert localSet != null : "Account set is null";
	}*/
	
	
	/*
	public void depositMoney(double sum,int accId,Person p)
	{
		Set<Account> localSet = accountList.get(p);
		if (localSet != null)
		{
			//iterator for Set
			for (Account a : localSet)
			{
				if (a.accId == accId)
				{
					a.addMoney(sum);
				}
			}
		}
	}*/
	
	public boolean checkPerson(Person person)
	{
		if (savingsAccountList.containsKey(person) || (spendingAccountList.containsKey(person)))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	@Override
	public String toString() {
		return "Bank [savingsAccountList=" + savingsAccountList + "] [spending=" + spendingAccountList + "]";
	}

}
