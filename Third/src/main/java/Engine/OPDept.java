package Engine;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;

// method used to signify the process of selling a product
public class OPDept {

	public OPDept()
	{
		
	}
	//method writing the invoice
		public void makeInvoice(Customer localCustomer,Product localProduct, String localQuantity)
		{
			if ((!localCustomer.equals(null)) && (!localProduct.equals(null)))
			{
				PrintWriter writer;
				try {
					writer = new PrintWriter("invoice.txt", "UTF-8");
					writer.println("Start of Invoice");
					writer.println("Company name: Warehouse S.R.L.");
					SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yy HH-mm-ss");
					Date date = new Date();
					writer.println("Date: " + dateFormat.format(date).toString());
					writer.println("Customer name: " + localCustomer.customerName);
					writer.println("Customer SSN: " + localCustomer.customerSSN);
					writer.println();
					writer.println("Product name: " + localProduct.productName);
					writer.println("Product ID: " + localProduct.productID);
					writer.println("Quantity bought: " + localQuantity);
					writer.println("End of Invoice");
					writer.close();
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (UnsupportedEncodingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
}
