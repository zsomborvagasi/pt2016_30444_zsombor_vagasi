package Engine;


//class to represent the customer and the belonging attributes
public class Customer {
	public String customerName;
	public Integer customerSSN; // social security number
	public String customerAddress;
	
	public Customer()
	{
		
	}
	
	public Customer(String custName, Integer custSSN, String custAddress)
	{
		customerName = custName;
		customerSSN = custSSN;
		customerAddress = custAddress;
	}

}
