package Engine;

import java.util.TreeMap;
//class to represent the collection of the products
// I have used TreeMap upon request from the professor
public class ProductList {

	public TreeMap<String,Product> productList;
	
	public ProductList()
	{
		productList = new TreeMap<String,Product>();
	}
	
	//method adds a product to the tree with the specified ID
	public void addProduct(String prodID,Product prod)
	{
		productList.put(prodID, prod);
	}
	
	//method deletes an element from the tree with the specified ID
	public void deleteProduct(String prodID)
	{
		productList.remove(prodID);
	}
	
	//method verifies if an existing id is existent in the tree
	public boolean checkExisting(String prodID)
	{
		if (productList.containsKey(prodID))
		{
			return true;
		}
		return false;
	}
	
	//method returns product with the specified id from the tree
	public Product getProduct(String prodID)
	{
		return productList.get(prodID);
	}
}
