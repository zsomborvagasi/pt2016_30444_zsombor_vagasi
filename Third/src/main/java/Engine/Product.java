package Engine;

//class to represent the product
public class Product {

	public String productName;
	public String productID;
	public String productQuantity;
	
	public Product()
	{
		
	}
	
	public Product(String prodName, String prodID, String prodQuantity)
	{
		setName(prodName);
		setID(prodID);
		setQuantity(prodQuantity);
	}
	
	private void setName(String name)
	{
		productName = name;
	}
	
	private void setID(String price)
	{
		productID = price;
	}
	
	private void setQuantity(String quantity)
	{
		productQuantity = quantity;
	}
}
