package Engine;
import java.util.ArrayList;
import java.util.Iterator;

// class to represent the collection of customers
// i have used simple object arraylist to create this collection
public class CustomerList {
	public ArrayList<Customer> customerList = new ArrayList<Customer>();
	
	public CustomerList()
	{
		customerList.clear();
	}
	
	// adda  customer to the arraylist
	public void addCustomer(Customer custom)
	{
		customerList.add(custom);
	}
	
	//returns the customer from the arraylist with teh specified SSN
	public Customer getCustomer(int i)
	{
		for (Customer cust : customerList)
		{
			if (i == cust.customerSSN)
			{
				return cust;
			}
		}
		//this is bad practice, shouldnt return null, but a verification will be done beforehand, so should 
		//arrive here
		return null;
	}
	
	//checks if a customer with the specified ssn is already existent in the list
	public boolean checkExisting(int ssn)
	{
		for (Customer cust : customerList)
		{
			if (ssn == cust.customerSSN)
			{
				return true;
			}
		}
		return false;	
	}
	
	//deletes the customer with the specified ssn from the list
	public void deleteCustomer(Integer ssn)
	{
		for (Iterator<Customer> iterator = customerList.iterator(); iterator.hasNext();) {
		    Customer cust = iterator.next();
		    if (cust.customerSSN == ssn) {
		        // Remove the current element from the iterator and the list.
		        iterator.remove();
		    }
		}
	}
	
	//updates the arraylist with the gives customer.
	public void updateCustomer(Customer customer)
	{
		int index = 0;
		for (Customer cust : customerList)
		{
			if (customer.customerSSN == cust.customerSSN)
			{
				customerList.set(index, customer);
			}
			index ++;
		}
	}
}
