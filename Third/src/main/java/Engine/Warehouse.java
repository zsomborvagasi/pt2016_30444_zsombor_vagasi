package Engine;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;

// class to contains the two lits of customers and products, symbolizes the company
public class Warehouse {
	public CustomerList customerL;
	public ProductList productL;
	
	public Warehouse()
	{
		customerL = new CustomerList();
		productL = new ProductList();
	}
	
}
