package Engine;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;

// class contains the method describing the selling of a product to a customer
public class Order {

	public Customer localCustomer;
	public Product localProduct;
	private String localQuantity;
	public Order()
	{
		localCustomer = null;
		localProduct = null;
		localQuantity = "0";
	}
	
	public Order(Customer cust, Product prod,String quantity)
	{
		localCustomer = cust;
		localProduct = prod;
		localQuantity = quantity;
	}
	
	// method will decrease the quantity of the specified product if it is possible, otherwise return a special
	//value whcih will be interpreted by the MainWindow class
	public Product sellProduct()
	{
		OPDept opDept = new OPDept();
		if (Integer.parseInt(localQuantity) <= Integer.parseInt(localProduct.productQuantity))
		{
			opDept.makeInvoice(localCustomer,localProduct,localQuantity);
			
			Integer newQuantity = Integer.parseInt(localProduct.productQuantity) - Integer.parseInt(localQuantity);
			localProduct.productQuantity = newQuantity.toString();
			return localProduct;
		}
		else
		{
			Integer returnValue = -1;
			localProduct.productQuantity = returnValue.toString();
			return localProduct;
		}
	}
}
