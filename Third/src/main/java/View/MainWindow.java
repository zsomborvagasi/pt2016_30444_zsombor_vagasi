package View;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import Engine.Customer;
import Engine.Order;
import Engine.Product;
import Engine.Warehouse;

import javax.swing.JButton;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.TreeMap;

// one and only class for graphical interface, contains the two J tables, all the necessary objects to enter 
// customer and product data, contains necessary objects to enter elements of a selling
public class MainWindow extends JFrame {

	private JPanel contentPane;
	private JTextField newCustomName;
	private JTextField newCustomSSN;
	private JTextField newCustomAddress;
	private JTextField delSSN;
	private JTextField updateName;
	private JTextField updateSSN;
	private JTextField updateAddress;
	
	private JTable table;
	
	private Warehouse wh;
	private JTable table_1;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;
	private JTextField textField_6;
	private JTextField textField_7;
	private JTextField textField_8;
	private JTextField textField_9;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainWindow frame = new MainWindow();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MainWindow() {
		super("Main");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1300, 760);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		wh = new Warehouse();
		
		newCustomName = new JTextField();
		newCustomName.setBounds(119, 156, 86, 20);
		contentPane.add(newCustomName);
		newCustomName.setColumns(10);
		
		newCustomSSN = new JTextField();
		newCustomSSN.setBounds(119, 199, 86, 20);
		contentPane.add(newCustomSSN);
		newCustomSSN.setColumns(10);
		
		newCustomAddress = new JTextField();
		newCustomAddress.setBounds(119, 246, 86, 20);
		contentPane.add(newCustomAddress);
		newCustomAddress.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("Name");
		lblNewLabel.setBounds(39, 159, 46, 14);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("SSN");
		lblNewLabel_1.setBounds(39, 202, 46, 14);
		contentPane.add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("Address");
		lblNewLabel_2.setBounds(39, 249, 46, 14);
		contentPane.add(lblNewLabel_2);
		
		// mouseClickEvent to handle addition to the customer list and to the jtable
		JButton btnAddCustomer = new JButton("Add Customer");
		btnAddCustomer.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				String customName = "";
				Integer customSSN = 0;
				String customAddress = "";
				
				customName = newCustomName.getText();
				customSSN = Integer.parseInt(newCustomSSN.getText());
				customAddress = newCustomAddress.getText();
				
				if (!wh.customerL.checkExisting(customSSN))
				{
					Customer newCustom = new Customer(customName,customSSN,customAddress);
					wh.customerL.addCustomer(newCustom);
					DefaultTableModel model = (DefaultTableModel) table.getModel();
					model.addRow(new Object[]{customName,customSSN.toString(),customAddress,"No."});
				}
				else
				{
					JOptionPane.showMessageDialog(null, "Customer with same SSN already exists.", "Warning!", JOptionPane.INFORMATION_MESSAGE);
				}
			}
		});
		btnAddCustomer.setBounds(63, 291, 142, 23);
		contentPane.add(btnAddCustomer);
		
		JLabel lblNewLabel_4 = new JLabel("SSN");
		lblNewLabel_4.setBounds(260, 159, 46, 14);
		contentPane.add(lblNewLabel_4);
		
		delSSN = new JTextField();
		delSSN.setBounds(330, 156, 86, 20);
		contentPane.add(delSSN);
		delSSN.setColumns(10);
		
		
		// mouseClickEvent to handle the deletion of a customer from the customer list and fromt eh jtable
		JButton btnNewButton = new JButton("Delete Customer");
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (!delSSN.getText().equals(""))
				{
				Integer deleteSSN = Integer.valueOf(delSSN.getText());
				if (wh.customerL.checkExisting(deleteSSN))
				{
					DefaultTableModel model = (DefaultTableModel) table.getModel();
					for (int i = 0; i < table.getRowCount();i++)
					{
						if (model.getValueAt(i, 1).equals(delSSN.getText()))
						{
							model.removeRow(i);
						}
					}
					wh.customerL.deleteCustomer(deleteSSN);
				}
				else
				{
					JOptionPane.showMessageDialog(null, "Customer with given SSN doesn't exist!", "Warning!", JOptionPane.INFORMATION_MESSAGE);
				}
			}
			}
		});
		btnNewButton.setBounds(287, 198, 129, 23);
		contentPane.add(btnNewButton);
		
		JLabel lblNewLabel_5 = new JLabel("Name");
		lblNewLabel_5.setBounds(477, 159, 46, 14);
		contentPane.add(lblNewLabel_5);
		
		updateName = new JTextField();
		updateName.setBounds(549, 156, 86, 20);
		contentPane.add(updateName);
		updateName.setColumns(10);
		
		JLabel lblNewLabel_6 = new JLabel("SSN");
		lblNewLabel_6.setBounds(477, 202, 46, 14);
		contentPane.add(lblNewLabel_6);
		
		updateSSN = new JTextField();
		updateSSN.setBounds(549, 199, 86, 20);
		contentPane.add(updateSSN);
		updateSSN.setColumns(10);
		
		JLabel lblNewLabel_7 = new JLabel("Address");
		lblNewLabel_7.setBounds(477, 249, 46, 14);
		contentPane.add(lblNewLabel_7);
		
		updateAddress = new JTextField();
		updateAddress.setBounds(549, 246, 86, 20);
		contentPane.add(updateAddress);
		updateAddress.setColumns(10);
		
		
		// mouseClickEvent to handle the update of a customer in the customer list and in the jtable
		JButton btnNewButton_1 = new JButton("Update Customer");
		btnNewButton_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				String customName = "";
				Integer customSSN = 0;
				String customAddress = "";
				
				customName = updateName.getText();
				customSSN = Integer.parseInt(updateSSN.getText());
				customAddress = updateAddress.getText();
				
				Customer cust = new Customer(customName,customSSN,customAddress);
				if (wh.customerL.checkExisting(customSSN))
				{
					DefaultTableModel model = (DefaultTableModel) table.getModel();
					for (int i = 0; i < wh.customerL.customerList.size();i++)
					{
						if (model.getValueAt(i, 1).equals(updateSSN.getText()))
						{
							model.setValueAt(customName, i, 0);
							model.setValueAt(customSSN, i, 1);
							model.setValueAt(customAddress, i, 2);
						}
					}
					wh.customerL.updateCustomer(cust);
				}
				else
				{
					JOptionPane.showMessageDialog(null, "Customer with given SSN doesn't exist!", "Warning!", JOptionPane.INFORMATION_MESSAGE);
				}
			}
		});
		btnNewButton_1.setBounds(500, 291, 135, 23);
		contentPane.add(btnNewButton_1);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(52, 11, 583, 105);
		contentPane.add(scrollPane);
		
		// creation of jtable for customer
		table = new JTable();
		table.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"Name", "SSN", "Address", "Has Ordered Before"
			}
		));
		scrollPane.setViewportView(table);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(730, 11, 464, 105);
		contentPane.add(scrollPane_1);
		
		
		//creation of jtable for product
		table_1 = new JTable();
		table_1.setModel(new DefaultTableModel(
			new Object[][] {
			},
			new String[] {
				"Product Name", "Product ID", "Product Quantity"
			}
		));
		table_1.getColumnModel().getColumn(0).setPreferredWidth(95);
		table_1.getColumnModel().getColumn(1).setPreferredWidth(103);
		table_1.getColumnModel().getColumn(2).setPreferredWidth(106);
		scrollPane_1.setViewportView(table_1);
		
		textField = new JTextField();
		textField.setBounds(800, 156, 86, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JLabel newProdName = new JLabel("Name");
		newProdName.setBounds(729, 159, 46, 14);
		contentPane.add(newProdName);
		
		textField_1 = new JTextField();
		textField_1.setBounds(800, 199, 86, 20);
		contentPane.add(textField_1);
		textField_1.setColumns(10);
		
		textField_2 = new JTextField();
		textField_2.setBounds(800, 246, 86, 20);
		contentPane.add(textField_2);
		textField_2.setColumns(10);
		
		
		// mouseClickEvent to handle the addition of new product to the product list and jtable
		JButton btnNewButton_2 = new JButton("Add Product");
		btnNewButton_2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				String prodName = textField.getText();
				String prodID = textField_1.getText();
				String prodQuantity = textField_2.getText();
				DefaultTableModel model = (DefaultTableModel) table_1.getModel();
				table_1.removeAll();
				
				if (!wh.productL.checkExisting(prodID))
				{
					Product prod = new Product(prodName,prodID,prodQuantity);
					wh.productL.addProduct(prodID, prod);
					model.addRow(new Object[]{prodName,prodID,prodQuantity});
				}
				else
				{
					JOptionPane.showMessageDialog(null, "Product with given ID already exists!", "Warning!", JOptionPane.INFORMATION_MESSAGE);
				}
			}
		});
		btnNewButton_2.setBounds(758, 291, 117, 23);
		contentPane.add(btnNewButton_2);
		
		JLabel newProdID = new JLabel("ID");
		newProdID.setBounds(730, 202, 46, 14);
		contentPane.add(newProdID);
		
		JLabel newProdQuantity = new JLabel("Quantity");
		newProdQuantity.setBounds(729, 249, 46, 14);
		contentPane.add(newProdQuantity);
		
		textField_3 = new JTextField();
		textField_3.setBounds(945, 156, 86, 20);
		contentPane.add(textField_3);
		textField_3.setColumns(10);
		
		JLabel lblNewLabel_3 = new JLabel("ID");
		lblNewLabel_3.setBounds(925, 159, 46, 14);
		contentPane.add(lblNewLabel_3);
		
		
		// mouseClickEvent to handle the deletion of a product from the product list and from the jtabel
		JButton btnNewButton_3 = new JButton("Delete Product");
		btnNewButton_3.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				String prodID = textField_3.getText();
				DefaultTableModel model = (DefaultTableModel) table_1.getModel();
				
				if (wh.productL.checkExisting(prodID))
				{
					
					for (int i = 0; i < table_1.getRowCount();i++)
					{
						if (model.getValueAt(i, 1).equals(textField_3.getText()))
						{
							model.removeRow(i);
						}
					}
					wh.productL.deleteProduct(prodID);
				}
				else
				{
					JOptionPane.showMessageDialog(null, "Product with given ID doesn't exists!", "Warning!", JOptionPane.INFORMATION_MESSAGE);
				}
			}
		});
		btnNewButton_3.setBounds(925, 198, 129, 23);
		contentPane.add(btnNewButton_3);
		
		textField_4 = new JTextField();
		textField_4.setBounds(1188, 156, 86, 20);
		contentPane.add(textField_4);
		textField_4.setColumns(10);
		
		textField_5 = new JTextField();
		textField_5.setBounds(1188, 199, 86, 20);
		contentPane.add(textField_5);
		textField_5.setColumns(10);
		
		
		// mouseClickEvent to handle the update of the product list and the jtable
		JButton btnNewButton_4 = new JButton("Update Product");
		btnNewButton_4.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				String prodName = textField_4.getText();
				String prodID = textField_5.getText();
				String prodQuantity = textField_6.getText();
				
				if (wh.productL.checkExisting(prodID))
				{
					wh.productL.deleteProduct(prodID);
					Product prod = new Product(prodName,prodID,prodQuantity);
					wh.productL.addProduct(prodID, prod);
					
					DefaultTableModel model = (DefaultTableModel) table_1.getModel();
					for (int i = 0; i < table_1.getRowCount();i++)
					{
						if (model.getValueAt(i, 1).equals(textField_5.getText()))
						{
							model.setValueAt(prodName, i, 0);
							model.setValueAt(prodID, i, 1);
							model.setValueAt(prodQuantity, i, 2);
						}
					}
				}
				else
				{
					JOptionPane.showMessageDialog(null, "Product with given ID doesn't exists!", "Warning!", JOptionPane.INFORMATION_MESSAGE);
				}
			}
		});
		btnNewButton_4.setBounds(1133, 291, 117, 23);
		contentPane.add(btnNewButton_4);
		
		textField_6 = new JTextField();
		textField_6.setBounds(1188, 246, 86, 20);
		contentPane.add(textField_6);
		textField_6.setColumns(10);
		
		JLabel lblNewLabel_8 = new JLabel("Name");
		lblNewLabel_8.setBounds(1114, 159, 46, 14);
		contentPane.add(lblNewLabel_8);
		
		JLabel lblNewLabel_9 = new JLabel("ID");
		lblNewLabel_9.setBounds(1114, 202, 46, 14);
		contentPane.add(lblNewLabel_9);
		
		JLabel lblNewLabel_10 = new JLabel("Quantity");
		lblNewLabel_10.setBounds(1114, 249, 46, 14);
		contentPane.add(lblNewLabel_10);
		
		textField_7 = new JTextField();
		textField_7.setBounds(201, 410, 86, 20);
		contentPane.add(textField_7);
		textField_7.setColumns(10);
		
		JLabel lblNewLabel_11 = new JLabel("Customer SSN");
		lblNewLabel_11.setBounds(94, 413, 81, 14);
		contentPane.add(lblNewLabel_11);
		
		textField_8 = new JTextField();
		textField_8.setBounds(397, 410, 86, 20);
		contentPane.add(textField_8);
		textField_8.setColumns(10);
		
		JLabel lblNewLabel_12 = new JLabel("Product ID");
		lblNewLabel_12.setBounds(325, 413, 62, 14);
		contentPane.add(lblNewLabel_12);
		
		textField_9 = new JTextField();
		textField_9.setBounds(608, 410, 86, 20);
		contentPane.add(textField_9);
		textField_9.setColumns(10);
		
		JLabel lblNewLabel_13 = new JLabel("Product Quantity");
		lblNewLabel_13.setBounds(500, 413, 98, 14);
		contentPane.add(lblNewLabel_13);
		
		
		// mouseClickEvent to handle the "selling" of a product to a customer
		// the ID of the product and the SSN of the customer are what specifies them.
		JButton btnNewButton_5 = new JButton("Buy");
		btnNewButton_5.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				if (wh.productL.checkExisting(textField_8.getText()) && wh.customerL.checkExisting(Integer.parseInt(textField_7.getText())))
				{
				
				Order order = new Order(wh.customerL.getCustomer(Integer.parseInt(textField_7.getText())),
						wh.productL.getProduct(textField_8.getText()),textField_9.getText());
				Product localProd = order.sellProduct();
				if (localProd.productQuantity.equals("-1"))
				{
					JOptionPane.showMessageDialog(null, "UNDERSTOCK!", "Warning!", JOptionPane.INFORMATION_MESSAGE);
				}
				else
				{
					wh.productL.deleteProduct(localProd.productID);
					wh.productL.addProduct(localProd.productID, localProd);
					
					DefaultTableModel model = (DefaultTableModel) table_1.getModel();
					for (int i = 0; i < table_1.getRowCount();i++)
					{
						if (model.getValueAt(i, 1).equals(localProd.productID))
						{
							model.setValueAt(localProd.productName, i, 0);
							model.setValueAt(localProd.productID, i, 1);
							model.setValueAt(localProd.productQuantity, i, 2);
						}
					}
					
					DefaultTableModel model1 = (DefaultTableModel) table.getModel();
					for (int i = 0; i < table.getRowCount();i++)
					{
						if (model1.getValueAt(i, 1).equals(textField_7.getText()))
						{
							model1.setValueAt("Yes.", i, 3);
						}
					}
					
					JOptionPane.showMessageDialog(null, "Specified number of product sold! Check the updated Product Table for remaining products!", "Warning!", JOptionPane.INFORMATION_MESSAGE);
				}
				}
				else
				{
					JOptionPane.showMessageDialog(null, "Customer or Product entered is non-existent!", "Warning!", JOptionPane.INFORMATION_MESSAGE);
				}
			}
		});
		btnNewButton_5.setBounds(394, 456, 89, 23);
		contentPane.add(btnNewButton_5);
	}
}
